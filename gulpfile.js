// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var sass         = require('gulp-sass');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');
var rename       = require('gulp-rename');
var cleanCSS     = require('gulp-clean-css');
var postcss      = require('gulp-postcss');
var imageResize  = require('gulp-image-resize');
var imagemin     = require('gulp-imagemin');
var webp         = require('gulp-webp');
var sourcemaps   = require('gulp-sourcemaps');
var browserSync  = require('browser-sync');
var eslint       = require('gulp-eslint');


// Lint Task
gulp.task('lint', function() {
    return gulp.src('app/js/*.js')
    .pipe(eslint({
        'rules': {
            'eqeqeq': 'off',
            'curly': 'warn',
            'quotes': ['warn', 'single']
        }
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

// Browser Sync Task
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'dist'
    },
  })
})

// Compile & Minify Sass
gulp.task('sass', function() {
    return gulp.src('app/css/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([ require('postcss-object-fit-images') ]))
        .pipe(sourcemaps.write('.'))
        .pipe(cleanCSS({compatibility: '*'}))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({
          stream: true
        }));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(['./app/js/plugins/*.js', './app/js/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('script.js'))
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/js'));
});

// Compress images
gulp.task('images', function() {
    return gulp.src('app/img/**')
      .pipe(gulp.dest('dist/img'))
      .pipe(webp())
      .pipe(imagemin())
      .pipe(gulp.dest('dist/img'));
});

gulp.task('root', function() {
    return gulp.src('app/root/**')
      .pipe(imagemin())
      .pipe(gulp.dest('dist'));
});

// Create thumbnails
gulp.task('resize', function () {
  gulp.src('app/img/axolotl-*')
    .pipe(imageResize({
      width : 20,
      height : 0,
      format : 'gif',
      upscale : false
    }))
    .pipe(gulp.dest('dist/img/thumbs'));
});

// Watch Files For Changes
gulp.task('watch', ['browserSync'], function() {
    gulp.watch('app/js/*.js', ['lint', 'scripts']);
    gulp.watch('app/css/**/*.scss', ['sass']);
    gulp.watch('app/img/**', ['images']);
    gulp.watch('app/root/**', ['root']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'images', 'root', 'resize', 'watch']);
