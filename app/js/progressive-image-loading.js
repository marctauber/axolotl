// Initially inspired by https://jmperezperez.com/medium-image-progressive-loading-placeholder/

function imageLoader () {

  var placeholders = document.querySelectorAll('.js-placeholder');

  function focusImage(placeholder) {

    var thumb = placeholder.querySelector('.js-thumb');
    var picture = placeholder.querySelector('.js-picture');
    thumb.classList.add('is-loaded');

    // 2: load large image
    var imgLargeWebP = document.createElement('source');
    var imgLargeJpg = document.createElement('source');
    var imgLargeFallback = new Image();
    imgLargeWebP.srcset = placeholder.dataset.large;
    imgLargeWebP.setAttribute('type','image/webp');
    imgLargeJpg.srcset = placeholder.dataset.large.replace(/\.[^\.]+$/, '.jpg');
    imgLargeJpg.setAttribute('type','image/jpeg');
    imgLargeFallback.src = placeholder.dataset.large.replace(/\.[^\.]+$/, '.jpg');
    imgLargeFallback.setAttribute('alt','');
    imgLargeFallback.onload = function () {
      picture.classList.add('is-loaded');
    };
    picture.appendChild(imgLargeWebP);
    picture.appendChild(imgLargeJpg);
    picture.appendChild(imgLargeFallback);

  }

  for (var i = 0, len = placeholders.length; i < len; i++) {
    (function(i){
      setTimeout(function(){
        focusImage(placeholders[i]);
      }, 1000 * i);
    }(i));
  }

}
