(function () {

  var body = document.querySelector('body');
  var main = document.querySelector('main');
  var pages = document.querySelectorAll('section');
  var header = document.querySelector('header');
  var links = document.querySelectorAll('.c-nav__list-link');
  var footers = document.querySelectorAll('.c-footer__list-link');

  window.addEventListener('load', function(){

      setTimeout (function() {
        body.className += 'is-loaded';
        for (var i=0; i<pages.length; i++) {
          pages[i].classList.add('is-collapsed');
        }
        objectFitImages();
      //  backgroundImages();
      }, 3000);

      document.body.addEventListener('touchstart', function(e){
          // alert('you touched me');
      }, false);

  }, false);

  [].forEach.call(document.getElementsByClassName('js-link'), function(el) {

    setTimeout (function() {
      el.classList.add('is-ready');
    }, 3000);

    el.addEventListener('click', function() {
      var target = el.getAttribute('href');
      var target = target.replace('#', '');
      var openPage = document.getElementById(target);
      var footerLink = document.getElementById(target + '-link-footer');
      openPage.classList.remove('is-away');
      openPage.classList.remove('is-collapsed');
      openPage.classList.add('is-focused');
      el.className += 'is-active is-away';
      footerLink.classList.add('is-active');
      footerLink.classList.remove('is-ready');
      // if (!el.classList.contains('is-focused')) {
      //   el.className += ' is-focused';
      //   imageLoader();
      // }
      // if (!pages.classList.contains('is-focused')) {
      //   el.className += ' is-focused';
      //   imageLoader();
      // }
      for (var i=0; i<pages.length; i++) {
        if (pages[i] !== openPage) {
            pages[i].classList.remove('is-focused');
            pages[i].classList.remove('is-collapsed');
            pages[i].classList.add('is-away');
        }
      }
      for (var i=0; i<links.length; i++) {
        links[i].classList.remove('is-active');
        links[i].classList.add('is-away');
      }
      openPage.classList.remove('is-away');
      openPage.classList.remove('is-collapsed');
      openPage.classList.add('is-focused');
      // if (el.classList.contains('is-collapsed')) {
      //   el.classList.remove('is-collapsed');
      // }
      if (!main.classList.contains('is-expanded')) {
        main.className += ' is-expanded';
      }
      if (!header.classList.contains('is-away')) {
        header.className += ' is-away';
      }
    })
  })

// add background images
  // function backgroundImages() {
  //   for (var i=0; i<pages.length; i++) {
  //     pages[i].style.backgroundImage = 'url(img/' + pages[i].id + '.jpg)';
  //   }
  // }


}());
